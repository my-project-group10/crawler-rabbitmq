#!/bin/sh

# Create Rabbitmq user
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RMQ_USERNAME $RMQ_PASSWORD 2>/dev/null ; \
rabbitmqctl set_user_tags $RMQ_USERNAME administrator ; \
rabbitmqctl set_permissions -p / $RMQ_USERNAME  ".*" ".*" ".*" ; \
echo "*** User '$RMQ_USERNAME' completed. ***" ; \
echo "*** Log in the WebUI at port 15672 (example: http:/localhost:15672) ***" ; \
rabbitmqadmin declare queue name=$RMQ_QUEUE durable=false arguments='{"x-queue-type":"classic"}') &

# $@ is used to pass arguments to the rabbitmq-server command.
# For example if you use it like this: docker run -d rabbitmq arg1 arg2,
# it will be as you run in the container rabbitmq-server arg1 arg2
rabbitmq-server $@
