FROM rabbitmq:3.8.9-management-alpine

# Define environment variables.
#ENV RABBITMQ_USER=${RABBITMQ_USER} RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD} RABBITMQ_QUEUE=${RABBITMQ_QUEUE}
ENV RABBITMQ_PID_FILE /var/lib/rabbitmq/mnesia/rabbitmq

COPY init.sh /init.sh
RUN chmod +x /init.sh
EXPOSE 15672

# Define default command
CMD ["/init.sh"]
